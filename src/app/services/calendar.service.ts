import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Reminder } from '../interfaces/reminder';


@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  private reminders: BehaviorSubject<Reminder[]> = new BehaviorSubject<Reminder[]>([]);

  create(data: Reminder): Reminder {
    const reminders = this.reminders.getValue();
    reminders.push(data);
    this.reminders.next(reminders);
    return data;
  }

  edit(data: Reminder): Reminder {
    const reminders = this.reminders.getValue();
    const reminderList = reminders.map(item => {
      if (item.id === data.id) {
        return data;
      }
      return item;
    });
    this.reminders.next(reminderList);
    return data;
  }

  list(): Observable<Reminder[]> {
    return this.reminders.asObservable();
  }

  delete(reminderId: number): boolean {
    const reminders = this.reminders.getValue();
    const index = reminders.findIndex(reminder => reminder.id === reminderId);
    reminders.splice(index, 1);
    this.reminders.next(reminders);
    return true;
  }
}
