import { TestBed } from '@angular/core/testing';

import { WeatherService } from './weather.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('WeatherService', () => {
  let service: WeatherService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(WeatherService);
    http = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the cities', () => {
    const spy = spyOn(http, 'get');
    service.getWeatherCities('rome');
    expect(spy).toHaveBeenCalledWith('http://api.openweathermap.org/geo/1.0/direct?q=rome&limit=5&appid=9081f17d1f41a71d9d305b63e4613808');
  });
});
