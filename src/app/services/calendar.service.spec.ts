import { TestBed } from '@angular/core/testing';

import { CalendarService } from './calendar.service';

describe('CalendarService', () => {
  let service: CalendarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalendarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should crate a reminder and add it to the reminder list', (done) => {
    const reminder = {
      id: 1,
      text: 'test',
      dateTime: new Date(),
      color: '',
      city: {
        name: 'city'
      }
    };
    service.create(reminder);
    service.list().subscribe(reminderList => {
      expect(reminderList).toEqual([reminder]);
      done();
    });
  });

  it('should edit a reminder on the reminder list', (done) => {
    const reminder = {
      id: 1,
      text: 'test',
      dateTime: new Date(),
      color: '',
      city: {
        name: 'city'
      },
    };
    service.create(reminder);
    service.create({ ...reminder, id: 2 });
    const newReminder = {
      ...reminder,
      text: 'Example',
      city: {
        name: 'London'
      }
    };
    service.edit(newReminder);
    service.list().subscribe(reminderList => {
      expect(reminderList).toEqual([newReminder, { ...reminder, id: 2 }]);
      done();
    });
  });

  it('should delete a reminder on the reminder list', (done) => {
    const reminder = {
      id: 1,
      text: 'test',
      dateTime: new Date(),
      color: '',
      city: {
        name: 'city'
      },
    };
    service.create(reminder);
    service.delete(reminder.id);
    service.list().subscribe(reminderList => {
      expect(reminderList).toEqual([]);
      done();
    });
  });
});
