import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OpenWeatherCity } from '../interfaces/open-weather-city';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private API_KEY = '9081f17d1f41a71d9d305b63e4613808';

  constructor(private http: HttpClient) {}

  getWeatherCities(city: string): Observable<OpenWeatherCity[]> {
    return this.http.get<OpenWeatherCity[]>(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=5&appid=${this.API_KEY}`);
  }
}
