import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarComponent } from './calendar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '../../modules/shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { Reminder } from '../../interfaces/reminder';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ReminderFormComponent } from '../reminder-form/reminder-form.component';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;
  let dialog: MatDialog;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarComponent, DatePipe ],
      imports: [
        HttpClientTestingModule,
        SharedModule,
        MatMenuModule,
        MatIconModule
      ]
    })
    .compileComponents();
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get reminder with the passed object', () => {
    const reminder: Reminder = {
      id: 1,
      text: 'Test',
      dateTime: new Date(2022, 6, 1, 14, 20),
      city: {
        name: 'London'
      },
      color: '#000000'
    };
    const text = component.getReminderText(reminder);
    expect(text).toEqual('Test - 02:20 PM | London');
  });

  it('should filter reminders by date', () => {
    const date = new Date(2022, 6, 5);
    const data: Reminder[] = [
      {
        id: 1,
        text: 'Test',
        dateTime: new Date(2022, 6, 4),
        city: {
          name: 'London'
        },
        color: '#000000'
      },
      {
        id: 2,
        text: 'Test name',
        dateTime: date,
        city: {
          name: 'Sliema'
        },
        color: '#531111'
      }
    ];
    component.reminders = data;
    const reminders = component.filterRemindersByDate(date);
    expect(reminders).toEqual([data[1]]);
  });

  it('should sum one month to the actual date', () => {
    component.actualDate = new Date(2022, 5, 2);
    component.changeToNextMonth();
    expect(component.actualDate.getMonth()).toEqual(6);
  });

  it('should subtract one month to the actual date', () => {
    component.actualDate = new Date(2022, 5, 2);
    component.changeToPrevMonth();
    expect(component.actualDate.getMonth()).toEqual(4);
  });

  it('should open reminder form', () => {
    const spy = spyOn(dialog, 'open');
    component.openReminderForm();
    expect(spy).toHaveBeenCalledWith(ReminderFormComponent, {
      data: undefined,
      disableClose: true
    });
  });

  it('should return reminder id when tracByNumber is called', () => {
    const reminder: Reminder = {
      id: 2,
      text: 'test',
      dateTime: new Date(),
      color: '#ffffff',
      city: {
        name: 'city name'
      }
    };
    const id = component.trackByReminder(0, reminder);
    expect(id).toEqual(2);
  });
});
