import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Reminder } from 'src/app/interfaces/reminder';
import { CalendarService } from 'src/app/services/calendar.service';
import { WeatherService } from 'src/app/services/weather.service';
import { MatDialog } from '@angular/material/dialog';
import { ReminderFormComponent } from '../reminder-form/reminder-form.component';
import { MONTH_NAMES } from './month-names';
import { WEEK_NAMES } from './week-names';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [DatePipe]
})
export class CalendarComponent implements OnInit, OnDestroy {

  onDestroy$ = new Subject<boolean>();
  weekNames = WEEK_NAMES;
  reminders: Reminder[] = [];
  monthList: Date[];
  actualDate: Date;
  private SUNDAY = 0;
  private SATURDAY = 6;

  constructor(
    private calendarService: CalendarService,
    private weatherService: WeatherService,
    private matDialog: MatDialog,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.actualDate = new Date();
    this.buildMonthList();
    this.calendarService.list()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((reminders: Reminder[]) => {
        this.reminders = reminders;
      });
  }

  /**
   * Builds the month list based on the global date
   */
  buildMonthList(): void {
    this.monthList = this.getAllDaysInMonth(this.actualDate.getFullYear(), this.actualDate.getMonth());
  }

  /**
   * Calculates days of the passed month based on 35 calendar days
   * also it validates the remaining days of the passed month
   * @param year year to get month
   * @param month current month
   * @private
   */
  private getAllDaysInMonth(year, month): Date[] {
    const date = new Date(year, month, 1);
    const dateList = [];
    let remainingDays = 0;
    const firstDayOfMonth = date.getDay();
    while (dateList.length < 35) {
      if (date.getDay() !== 0) {
        const newDate = new Date(date);
        while (remainingDays < firstDayOfMonth) {
          newDate.setDate(newDate.getDate() - 1);
          dateList.unshift(new Date(newDate));
          remainingDays++;
        }
      }
      dateList.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return dateList;
  }

  /**
   * Opens the reminder modal
   * @param reminder object
   */
  openReminderForm(reminder?: Reminder): void {
    this.matDialog.open(ReminderFormComponent, {
      data: reminder,
      disableClose: true
    });
  }

  /**
   * Gets the reminder description based on a reminder
   * @param reminder object
   */
  getReminderText(reminder: Reminder): string {
    const time = this.datePipe.transform(reminder.dateTime, 'hh:mm a');
    return `${reminder.text} - ${time} | ${reminder.city?.name}`;
  }

  /**
   * Validates if the passed date is a weekend day and if the passed month is
   * different from the actual date
   * @param date passed date
   */
  validateDay(date: Date): string {
    let classes = '';
    const day = date.getDay();
    if (day === this.SUNDAY || day === this.SATURDAY) {
      classes += 'calendar-cell--weekend-day ';
    }
    if (this.actualDate.getMonth() !== date.getMonth()) {
      classes += 'calendar-cell--other-month-day';
    }
    return classes;
  }

  /**
   * Gets the reminder list based on a date
   * @param date the current date to validate
   */
  filterRemindersByDate(date: Date): Reminder[] {
    if (!this.reminders.length) {
      return [];
    }
    return this.reminders.filter(value => {
      const reminderDate = new Date(value.dateTime).toLocaleDateString();
      if (reminderDate === date.toLocaleDateString()) {
        return value;
      }
    });
  }

  /**
   * Gets the current month name based on the actual date
   */
  getCurrentMonthName(): string {
    const month = this.actualDate.getMonth();
    return MONTH_NAMES[month];
  }

  /**
   * Changes the actual month to the next one
   */
  changeToNextMonth(): void {
    this.actualDate.setMonth(this.actualDate.getMonth() + 1);
    this.buildMonthList();
  }

  /**
   * Changes the actual month to the previous one
   */
  changeToPrevMonth(): void {
    this.actualDate.setMonth(this.actualDate.getMonth() - 1);
    this.buildMonthList();
  }

  /**
   * Tracks by reminder's id the passed reminder
   * @param _ index number
   * @param reminder reminder
   */
  trackByReminder(_: number, reminder: Reminder): number {
    return reminder.id;
  }

  ngOnDestroy() {
    this.onDestroy$.next(true);
    this.onDestroy$.complete();
  }

}
