import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReminderFormComponent } from './reminder-form.component';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { OpenWeatherCity } from '../../interfaces/open-weather-city';
import { CalendarService } from '../../services/calendar.service';
import { WeatherService } from '../../services/weather.service';
import { SharedModule } from '../../modules/shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ReminderFormComponent', () => {
  let component: ReminderFormComponent;
  let fixture: ComponentFixture<ReminderFormComponent>;
  let calendarService: CalendarService;
  let weatherService: WeatherService;
  let dialogRef: MatDialogRef<ReminderFormComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReminderFormComponent ],
      imports: [
        MatDialogModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatAutocompleteModule,
        SharedModule,
        NoopAnimationsModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: { close: () => {} } },
      ]
    })
    .compileComponents();

    calendarService = TestBed.inject(CalendarService);
    weatherService = TestBed.inject(WeatherService);
    dialogRef = TestBed.inject(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReminderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create form without data', () => {
    component.data = null;
    component.createForm();
    expect(component.reminderForm.getRawValue()).toEqual({
      text: null,
      dateTime: null,
      color: '#2F74B5',
      city: null,
    });
  });

  it('should create form with passed data', () => {
    const date = new Date(2022, 6, 1);
    component.data = {
      id: 1,
      text: 'Test',
      dateTime: date,
      color: '#CCCCCC',
      city: {
        name: 'London',
      }
    };
    component.createForm();
    expect(component.reminderForm.getRawValue()).toEqual({
      text: 'Test',
      dateTime: date,
      color: '#CCCCCC',
      city: {
        name: 'London',
      }
    });
  });

  it('should invalidate the form when text field has more than 30 chars', () => {
    component.data = null;
    component.createForm();
    component.reminderForm.get('text').setValue('Lorem ipsum dolor sit amet, consectetur adipisicing elit');
    expect(component.reminderForm.invalid).toBeTrue();
  });

  it('should invalidate the form when all fields have no data', () => {
    component.data = null;
    component.createForm();
    expect(component.reminderForm.invalid).toBeTrue();
  });

  it('should set city value to the form when onSelectCity gets fired', () => {
    const input: OpenWeatherCity = {
      name: 'London',
      lat: 49.564665,
      lon: 3.620686,
      country: 'GB',
      state: 'England'
    };
    component.onSelectCity(input);
    expect(component.reminderForm.get('city').value).toEqual({
      name: 'London',
    });
  });

  it('should create a new reminder', () => {
    component.data = null;
    const expectedValue = {
      text: 'Test',
      dateTime: new Date(2022, 6, 1),
      color: '#CCCCCC',
      city: {
        name: 'Barcelona',
        lat: '49.564665',
        lon: '3.620686',
      }
    };
    component.reminderForm.patchValue(expectedValue);
    const spy = spyOn(calendarService, 'create').and.returnValue({
      id: 1,
      ...expectedValue
    });
    component.onSubmit();
    expect(spy).toHaveBeenCalled();
  });

  it('should update an existing reminder', () => {
    const expectedValue = {
      text: 'Test',
      dateTime: new Date(2022, 6, 1),
      color: '#CCCCCC',
      city: {
        name: 'Barcelona',
        lat: '49.564665',
        lon: '3.620686',
      }
    };
    component.data = {
      id: 1,
      ...expectedValue
    };
    const spy = spyOn(calendarService, 'edit').and.returnValue({
      id: 1,
      ...expectedValue
    });
    component.onSubmit();
    expect(spy).toHaveBeenCalled();
  });

  it('should remove reminder', () => {
    const spy = spyOn(calendarService, 'delete').and.returnValue(true);
    component.data = {
      id: 1,
      text: 'test',
      dateTime: new Date(),
      color: '',
      city: {
        name: 'city'
      }
    };
    component.removeReminder();
    expect(spy).toHaveBeenCalledWith(1);
  });

  it('should close modal', () => {
    const spy = spyOn(dialogRef, 'close');
    component.closeModal();
    expect(spy).toHaveBeenCalled();
  });
});
