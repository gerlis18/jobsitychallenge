import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Reminder } from 'src/app/interfaces/reminder';
import { CalendarService } from '../../services/calendar.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, filter, switchMap, takeUntil } from 'rxjs/operators';
import { WeatherService } from '../../services/weather.service';
import { OpenWeatherCity } from '../../interfaces/open-weather-city';

@Component({
  selector: 'app-reminder-form',
  templateUrl: './reminder-form.component.html',
  styleUrls: ['./reminder-form.component.scss']
})
export class ReminderFormComponent implements OnInit, OnDestroy {

  onDestroy$ = new Subject<boolean>();
  reminderForm: FormGroup;
  isUpdate: boolean;
  cities$: Observable<OpenWeatherCity[]>;
  cityControl = new FormControl('');


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Reminder,
    private formBuilder: FormBuilder,
    private calendarService: CalendarService,
    private weatherService: WeatherService,
    private dialogRef: MatDialogRef<ReminderFormComponent>
    ) { }

  ngOnInit(): void {
    this.isUpdate = Boolean(this.data);
    this.createForm();
    this.onCityChanges();
  }

  /**
   * Creates the reminders form
   */
  createForm(): void {
    this.reminderForm = this.formBuilder.group({
      text: [this.data?.text, [Validators.required, Validators.maxLength(30)]],
      dateTime: [this.data?.dateTime, [Validators.required]],
      color: [this.data?.color || '#2F74B5', [Validators.required]],
      city: [this.data?.city, [Validators.required]],
    });
    this.cityControl.setValue(this.data?.city?.name);
  }

  /**
   * Validates every type on the city field to get a city list based on the
   * input value
   */
  onCityChanges(): void {
    this.cities$ = this.cityControl
      .valueChanges
      .pipe(
        takeUntil(this.onDestroy$),
        filter(text => text && text.length > 3),
        debounceTime(100),
        switchMap(text => {
          return this.weatherService.getWeatherCities(text);
        })
      );
  }

  /**
   * Assigns the selected city on the autocomplete field
   * @param city city object
   */
  onSelectCity(city: OpenWeatherCity) {
    this.reminderForm.get('city').setValue({
      name: city.name
    });
  }

  /**
   * Validates if the modal has data to edit otherwise
   * it creates a new reminder
   */
  onSubmit(): void {
    if (this.data) {
      this.calendarService.edit({
        id: this.data.id,
        ...this.reminderForm.value
      });
    } else {
      this.calendarService.create({
        id: new Date().getMilliseconds(),
        ...this.reminderForm.value
      });
    }
    this.dialogRef.close();
  }

  /**
   * Removes a reminder from the reminder list given
   * the reminder id
   */
  removeReminder(): void {
    this.calendarService.delete(this.data.id);
    this.dialogRef.close();
  }

  /**
   * Closes the current modal
   */
  closeModal(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.onDestroy$.next(true);
    this.onDestroy$.complete();
  }

}
