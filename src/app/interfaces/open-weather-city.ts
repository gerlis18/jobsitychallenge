export interface OpenWeatherCity {
  name: string;
  lat: number;
  lon: number;
  country: string;
  state: string;
}
