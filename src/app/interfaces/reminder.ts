export interface Reminder {
  id: number;
  text: string;
  dateTime: Date;
  color: string;
  city?: City;
}

export interface City {
  name: string;
}
